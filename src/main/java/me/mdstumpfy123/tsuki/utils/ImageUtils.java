package me.mdstumpfy123.tsuki.utils;

import com.jagrosh.jdautilities.command.CommandEvent;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;

public class ImageUtils {
    /**
     * A function to put a user's avatar on an image
     *
     * @param event - CommandEvent of the command
     * @param avatarurl - ...
     * @param imageurl - ...
     * @param lx - x of the top left corner
     * @param ly - y of the top left corner
     * @param bx - x of the bottom right corner
     * @param by - y of the bottom left corner
     * @param fillArea - if to fill the area behind the avatar
     * @param fillcolor - what color to fill it with
     */
    public static void putAvatarOnImage(CommandEvent event, String avatarurl, String imageurl, int lx, int ly, int bx, int by, boolean fillArea, Color fillcolor) {
        try {
            HttpURLConnection conn= (HttpURLConnection) new URL(avatarurl+"?size=256").openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0");
            BufferedImage image = ImageIO.read(conn.getInputStream());
            conn.disconnect();
            conn = (HttpURLConnection) new URL(imageurl).openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0");
            BufferedImage bg = ImageIO.read(conn.getInputStream());
            conn.disconnect();
            Graphics graphics = bg.createGraphics().create();
            if (fillArea) {
                graphics.setColor(fillcolor);
                graphics.fillRect(lx, ly, bx - lx, by - ly);
            }
            graphics.drawImage(image, lx, ly, bx, by, 0, 0, 256, 256, null);
            File savedimage = new File(System.getProperty("user.dir")+"\\"+event.getAuthor().getId() +".PNG");
            ImageIO.write(bg, "PNG", savedimage);
            event.getChannel().sendFile(savedimage).queue((ev)->savedimage.delete());
        } catch (Exception gulp) {
            gulp.printStackTrace();
        }
    }
    /**
     *  Filters an image to be transparent with the alpha given
     *  then puts the image on the avatar
     *
     * @param event - CommandEvent of the command
     * @param avatarurl - ...
     * @param imageurl - ...
     * @param alpha - transparency (check google for argb)
     */
    public static void filterImageOnAvatar(CommandEvent event, String avatarurl, String imageurl, float alpha) {
        try {
            HttpURLConnection conn= (HttpURLConnection) new URL(avatarurl+"?size=256").openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0");
            BufferedImage image = ImageIO.read(conn.getInputStream());
            conn.disconnect();
            conn = (HttpURLConnection) new URL(imageurl).openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0");
            BufferedImage bg = ImageIO.read(conn.getInputStream());
            conn.disconnect();
            BufferedImage bgfade = new BufferedImage(bg.getWidth(), bg.getHeight(), BufferedImage.TYPE_INT_ARGB);
            Graphics2D graphics = bgfade.createGraphics();
            AlphaComposite composite = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha);
            graphics.setComposite(composite);
            graphics.drawImage(bg, 0, 0 ,null);
            Graphics g = image.createGraphics();
            g.drawImage(bgfade, 0, 0, 256, 256, 0, 0, bg.getWidth(), bg.getHeight(), null);
            File savedimage = new File(System.getProperty("user.dir")+"\\"+event.getAuthor().getId()+".PNG");
            ImageIO.write(image, "PNG", savedimage);
            event.getChannel().sendFile(savedimage).queue((ev)->savedimage.delete());
        } catch (Exception gulp) {
            gulp.printStackTrace();
        }
    }
}