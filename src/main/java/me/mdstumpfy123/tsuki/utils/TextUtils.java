package me.mdstumpfy123.tsuki.utils;

import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public class TextUtils {
    public static String trackToString(AudioTrack track) {
        return String.format("**%s** by **%s**["+ durationToString(track.getInfo().length) +"]", track.getInfo().title, track.getInfo().author);
    }

    public static String durationToString(long duration){
        String s = "";
        if ((int)TimeUnit.MILLISECONDS.toHours(duration)!=0){
            s += String.format("%02d:%02d:%02d", (int) TimeUnit.MILLISECONDS.toHours(duration), (int)TimeUnit.MILLISECONDS.toMinutes(duration)%60, (int)TimeUnit.MILLISECONDS.toSeconds(duration)%60);
        } else {
            s += String.format("%02d:%02d", (int)TimeUnit.MILLISECONDS.toMinutes(duration)%60, (int)TimeUnit.MILLISECONDS.toSeconds(duration)%60);
        }
        return s;
    }
}
