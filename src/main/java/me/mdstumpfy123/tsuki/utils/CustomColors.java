package me.mdstumpfy123.tsuki.utils;

import java.awt.Color;

public class CustomColors {

    public final static Color tsukiMagenta = new Color(233,30,99);
    public final static Color tsukiBug = new Color(221, 46, 68);
    public final static Color tsukiSuccess = new Color(8, 222, 6);
    public final static Color tsukiMute = new Color(250, 193, 12);

}
