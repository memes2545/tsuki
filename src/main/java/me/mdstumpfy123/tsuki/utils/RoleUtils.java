package me.mdstumpfy123.tsuki.utils;

import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Role;

import java.util.List;

public class RoleUtils {
    public static boolean isRoleSubset(Role role1, Role role2) {
        for (Permission permission : role1.getPermissions()) {
            if (!role2.getPermissions().contains(permission)) {
                return false;
            }
        }
        return true;
    }

    public static boolean areRolesSubset(List<Role> roles, Role role) {
        if (role == null){
            for (Role role1 : roles) {
                if (role1.hasPermission(Permission.MANAGE_SERVER)) return true;
            }
            return false;
        }
        for (Role role1: roles) {
            if (isRoleSubset(role, role1)) {
                return true;
            }
        }
        return false;
    }
}
