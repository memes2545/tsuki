package me.mdstumpfy123.tsuki.events;

import me.mdstumpfy123.tsuki.Config;
import me.mdstumpfy123.tsuki.utils.CustomColors;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.events.guild.GuildLeaveEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import org.discordbots.api.client.DiscordBotListAPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GuildLeaveLogEvent extends ListenerAdapter {
    @Override
    public void onGuildLeave(GuildLeaveEvent event) {
        JDA jda = event.getJDA();
        TextChannel guildLog =  jda.getTextChannelById("432206503596916736");
        Logger log = LoggerFactory.getLogger(GuildJoinLogEvent.class);
        Guild guild = event.getGuild();

        log.info("Left guild " + guild.getName());

        EmbedBuilder guildEmbed = new EmbedBuilder();
        guildEmbed.setAuthor("Guild Removed!", null, guild.getIconUrl());
        guildEmbed.setColor(CustomColors.tsukiBug);
        guildEmbed.appendDescription("Aww. Someone removed me..");
        guildEmbed.addField("Guild Name", guild.getName(), true);
        guildEmbed.addField("Guild ID", guild.getId(), true);
        guildEmbed.addField("Guild Owner ID", guild.getOwner().getUser().getId(), true);
        guildEmbed.addField("Guild Member Count", String.valueOf(guild.getMemberCache().size()), true);
        guildEmbed.setThumbnail(guild.getIconUrl());

        guildLog.sendMessage(guildEmbed.build()).queue();

        jda.getPresence().setGame(Game.watching("for your commands! tsuki.help ["+jda.getGuilds().size()+"]"));

        DiscordBotListAPI api = new DiscordBotListAPI.Builder()
                .token(Config.DBLTOKEN)
                .build();

        String botId = "430101737769009162";
        int serverCount = jda.getGuilds().size();

        api.setStats(botId, serverCount);

    }
}
