package me.mdstumpfy123.tsuki.events;

import me.mdstumpfy123.tsuki.Config;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.events.ReadyEvent;
import org.discordbots.api.client.DiscordBotListAPI;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class DBLStatsUpdate {
    private DiscordBotListAPI api;

    public DBLStatsUpdate(JDA jda){
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        service.scheduleAtFixedRate(()->updateStats(jda), 0, 30, TimeUnit.MINUTES);
         api = new DiscordBotListAPI.Builder()
                .token(Config.DBLTOKEN)
                .build();
    }

    private void updateStats(JDA jda) {
        String botId = "430101737769009162";
        int serverCount = (int) jda.getGuildCache().size();
        api.setStats(botId, serverCount);
    }
}
