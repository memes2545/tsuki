package me.mdstumpfy123.tsuki.api;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.command.CommandListener;
import net.dv8tion.jda.core.exceptions.InsufficientPermissionException;

public class TsukiEventListener implements CommandListener {
    @Override
    public void onCommandException(CommandEvent event, Command command, Throwable throwable) {
        if (throwable instanceof InsufficientPermissionException) {
            event.reply(throwable.getMessage());
        }
    }
}
