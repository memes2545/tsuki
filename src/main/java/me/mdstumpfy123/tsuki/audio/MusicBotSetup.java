package me.mdstumpfy123.tsuki.audio;

import me.mdstumpfy123.tsuki.utils.SearchUtils;
import com.sedmelluq.discord.lavaplayer.format.AudioDataFormat;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Guild;

import java.util.HashMap;
import java.util.Map;

/**
 * A class to help with the making of the music bot.
 * manager of all GuildManagers.
 */
public class MusicBotSetup {
    private final Map<Long, GuildManager> managers = new HashMap<>();
    public AudioPlayerManager manager;
    public SearchUtils utils = new SearchUtils();

    /**
     * Constructor that gives basic audio output format configuration.
     * @param manager Audio player manager from where to take the player
     */
    public MusicBotSetup(AudioPlayerManager manager) {
        this.manager = manager;
        AudioSourceManagers.registerRemoteSources(manager);
        manager.getConfiguration().setOutputFormat(new AudioDataFormat(2, 44100, 960, AudioDataFormat.Codec.PCM_S16_BE));
    }

    /**
     * A function that returns for every guild the GuildManager
     * @param guild the guild you want to take the manager from
     * @return The manager of the guild
     */
    public synchronized GuildManager getManagerFromGuild(Guild guild) {
        long id = guild.getIdLong();
        GuildManager gmanager = this.managers.get(id);
        if (gmanager == null) {
            JDA jda = guild.getJDA();
            gmanager = new GuildManager(jda, guild);
            managers.put(id, gmanager);
        }

        return gmanager;
    }
}
