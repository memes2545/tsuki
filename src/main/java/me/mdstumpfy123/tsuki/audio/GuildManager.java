package me.mdstumpfy123.tsuki.audio;

import me.mdstumpfy123.tsuki.utils.TextUtils;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventAdapter;
import com.sedmelluq.discord.lavaplayer.source.beam.BeamAudioSourceManager;
import com.sedmelluq.discord.lavaplayer.source.http.HttpAudioSourceManager;
import com.sedmelluq.discord.lavaplayer.source.soundcloud.SoundCloudAudioSourceManager;
import com.sedmelluq.discord.lavaplayer.source.twitch.TwitchStreamAudioSourceManager;
import com.sedmelluq.discord.lavaplayer.source.youtube.YoutubeAudioSourceManager;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason;
import com.sedmelluq.discord.lavaplayer.track.playback.AudioFrame;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.audio.AudioSendHandler;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.Event;
import net.dv8tion.jda.core.managers.AudioManager;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class GuildManager extends AudioEventAdapter implements AudioSendHandler {
    private AudioFrame last;
    private final Guild guild;
    public final JDA jda;
    private TextChannel channel;
    public AudioPlayerManager pmanager;
    private boolean loop;
    public final AudioPlayer player;
    public final LinkedList<SongInfo> queue;
    private SongInfo current;
    public final LinkedList<SongInfo> lqueue;
    private boolean loopq;

    public GuildManager(JDA jda, Guild guild) {
        this.guild = guild;
        pmanager = new DefaultAudioPlayerManager();
        pmanager.registerSourceManager(new YoutubeAudioSourceManager());
        pmanager.registerSourceManager(new SoundCloudAudioSourceManager());
        pmanager.registerSourceManager(new TwitchStreamAudioSourceManager());
        pmanager.registerSourceManager(new BeamAudioSourceManager());
        pmanager.registerSourceManager(new HttpAudioSourceManager());
        this.jda = jda;
        AudioManager manager = guild.getAudioManager();
        this.player = pmanager.createPlayer();
        player.addListener(this);
        this.queue = new LinkedList<>();
        this.lqueue = new LinkedList<>();
        manager.setSendingHandler(this);
    }

    public void setChannel(TextChannel channel) {
        this.channel = channel;
    }

    public void nextSong(AudioTrack track, User requester) {
        SongInfo info = new SongInfo(requester, track);
        if (!player.startTrack(track, true)) {
            queue.offer(info);
            lqueue.offer(info);
            channel.sendMessage("Enqueued: " + TextUtils.trackToString(track)).queue();
        } else {
            current = info;
            lqueue.offer(info);
        }
    }

    public void nextSong(AudioTrack track, User requester, String s) {
        SongInfo info = new SongInfo(requester, track);
        if (!player.startTrack(track, true)) {
            queue.offer(info);
            lqueue.offer(info);
        } else {
            current = info;
            lqueue.offer(info);
        }

    }

    public void stop() {
        if (player.getPlayingTrack() != null || player.isPaused())
            channel.sendMessage("Player stopped. " + (queue.isEmpty() ? "" : "`" + queue.size() + "` songs have been cleared from the queue." )).queue();
        this.channel = null;
        this.current = null;
        player.stopTrack();
        queue.clear();
        lqueue.clear();

        if (guild.getAudioManager().isConnected())
            guild.getAudioManager().closeAudioConnection();
    }

    public void skip(Member member, CommandEvent event) {
        MessageChannel channel = event.getChannel();
        if (current.skips.size()==5 || (!current.skips.contains(member.getUser())&&current.skips.size()==4)) {
            if (queue.isEmpty()&&!loopq) {
                stop();
            } else if (loopq) {
                channel.sendMessage("Skipping track: " + TextUtils.trackToString(current.track)).queue();
                if (queue.isEmpty()) {
                    queue.addAll(lqueue);
                }
                player.stopTrack();
                current = queue.poll();
                player.startTrack(current.track.makeClone(), false);
            } else {
                channel.sendMessage("Skipping track: " + TextUtils.trackToString(current.track)).queue();
                player.stopTrack();
                current = queue.poll();
                player.startTrack(current.track.makeClone(), false);
            }
        } else if (current.skips.contains(member.getUser())) {
            channel.sendMessage(String.format("You already voted to skip. [%d/5] votes were made", current.skips.size())).queue();
        } else if (!current.skips.contains(member.getUser())){
            channel.sendMessage(String.format("Added your vote to skips [%d/5]", current.skips.size())).queue();
        }
    }

    public void skip(Member member, CommandEvent event, int songs) {
        if (queue.size() < songs) {
            event.reply("More songs to skip then there are in the queue!");
        } else {
            for (int i =0; i<songs-1; i++){
                current = queue.poll();
            }
            event.reply("Skipped `"+songs+"` songs.");
            player.startTrack(current.track.makeClone(), false);
        }



    }

    @Override
    public void onTrackStart(AudioPlayer player, AudioTrack track) {
        channel.sendMessage("Now playing: " + TextUtils.trackToString(track)).queue();
    }

    @Override
    public void onTrackEnd(AudioPlayer player, AudioTrack track, AudioTrackEndReason endReason) {
        this.current = null;
        if (endReason.mayStartNext) {
            if (queue.isEmpty()) {
                if (loopq) {
                    queue.addAll(lqueue);
                    current = queue.poll();
                    player.startTrack(current.track.makeClone(), false);
                } else if (loop) {
                    player.startTrack(track.makeClone(), false);
                } else {
                    this.channel = null;
                }
            }  else {
                current = queue.poll();
                player.startTrack(current.track.makeClone(), false);
            }
        }
    }

    public void setCurrent(SongInfo current) {
        this.current = current;
    }

    public boolean isLoop() {
        return loop;
    }

    public boolean isLoopq() {
        return loopq;
    }

    public SongInfo getCurrent() {
        return current;
    }

    public long getPosition() {
        return current.track.getPosition();
    }

    public boolean isPlaying(){
        return getCurrent() != null;
    }

    public void setLoop(boolean loop) {
        this.loop = loop;
    }

    public void setLoopq(boolean loopq) {
        this.loopq = loopq;
    }

    public void shuffle() {
        Collections.shuffle(queue);
    }

    public void reverse() {Collections.reverse(queue); Collections.reverse(lqueue);}

    public void seek(long seconds) {
        current.track.setPosition(TimeUnit.SECONDS.toMillis(seconds));
    }



    @Override
    public boolean canProvide() {
        if (last == null) {
            last = player.provide();
        }
        return last != null;
    }

    @Override
    public byte[] provide20MsAudio() {
        if (last == null) {
            last = player.provide();
        }

        byte[] data = last != null ? last.data : null;
        last = null;

        return data;
    }

    @Override
    public boolean isOpus() {
        return true;
    }
}
