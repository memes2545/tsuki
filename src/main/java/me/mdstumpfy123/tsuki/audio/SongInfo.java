package me.mdstumpfy123.tsuki.audio;

import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import net.dv8tion.jda.core.entities.User;

import java.util.HashSet;
import java.util.Set;

public class SongInfo {
    public Set<User> skips;
    public User requester;
    public AudioTrack track;

    public SongInfo(User requester, AudioTrack track){
        this.requester = requester;
        this.track = track;
        this.skips = new HashSet<>();
    }
}
