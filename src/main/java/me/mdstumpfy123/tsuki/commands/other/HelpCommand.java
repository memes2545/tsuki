package me.mdstumpfy123.tsuki.commands.other;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandClientBuilder;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.core.EmbedBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class HelpCommand implements Consumer<CommandEvent> {
    @Override
    public void accept(CommandEvent event) {
        Logger log = LoggerFactory.getLogger(HelpCommand.class);
        if (event.getArgs().equals("")) {
            event.reply("You can ask `tsuki.help <Module>` to get the commands of the module or `tsuki.help <Command>` for more help on a command!\n" +
                    "**I sent you the full documentation in the DMs**\n" +
                    "For further assistance join the support server: " + event.getClient().getServerInvite());
            String currcat = "";
            StringBuilder message = new StringBuilder();
            message.append("Thanks for using Tsuki! Here's your documentation kind sir!\n");
            for (Command command : event.getClient().getCommands()) {
                if (command.getCategory()==null)
                    continue;
                if (!command.getCategory().getName().equals(currcat)) {
                    currcat = command.getCategory().getName();
                    message.append("\nModule **").append(currcat).append("**:\n").append("`").append(command.getName()).append("` ").append(command.getHelp()).append("\n");
                } else {
                    message.append("`").append(command.getName()).append("` ").append(command.getHelp()).append("\n");
                }
            }
            event.replyInDm(message.toString());
        } else {
            // Check if the arguements are a command
            for (Command comm : event.getClient().getCommands()) {
                Command command = (Command) comm;
                if (command.getName().equals(event.getArgs())) {
                    EmbedBuilder builder = new EmbedBuilder();
                    builder.setTitle("Command " +command.getName());
                    builder.setDescription(command.getHelp());
                    builder.setColor(Color.CYAN);
                    event.reply(builder.build());
                    return;
                }
            }
            // it is not a command let's search for a category.
            List<Command> commands= new ArrayList<>();
            for (Command command:event.getClient().getCommands()) {
                if (command.getCategory() == null) {
                    continue;
                }
                if (command.getCategory().getName().equals(event.getArgs())) {
                    commands.add(command);
                }
            }
            if (!commands.isEmpty()) {
                StringBuilder s = new StringBuilder("Module **" + commands.get(0).getCategory().getName() + "**:\n");
                for (Command command : commands) {
                    s.append("`^").append(command.getName()).append("`  ").append(command.getHelp()).append("\n");
                }
                s.append("\nUse `tsuki.help <Command>` for more info about each command!");
                event.reply(s.toString());
            } else {
                event.reply("Module or command " + event.getArgs() + " not found.\nuse `tsuki.help` for more info.");
            }
        }

    }
}
