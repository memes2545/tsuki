package me.mdstumpfy123.tsuki.commands.other;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.mdstumpfy123.tsuki.utils.CustomColors;
import net.dv8tion.jda.core.EmbedBuilder;

public class DonateCommand extends Command {
    public DonateCommand() {
        this.name = "donate";
        this.help = "Gives you the patreon link!";
        this.category = new Category("other");
    }

    @Override
    protected void execute(CommandEvent event) {
        EmbedBuilder donateEmbed =  new EmbedBuilder()
                .setAuthor("Donate please!", null, event.getGuild().getSelfMember().getUser().getEffectiveAvatarUrl())
                .appendDescription("[Click to donate!](https://www.patreon.com/tsukibot)")
                .setColor(CustomColors.tsukiMagenta);

        event.reply(donateEmbed.build());
    }
}
