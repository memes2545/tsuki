package me.mdstumpfy123.tsuki.commands.other;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.mdstumpfy123.tsuki.utils.CustomColors;
import net.dv8tion.jda.core.EmbedBuilder;

public class SupportServerCommand extends Command {
    public SupportServerCommand(){
        this.aliases = new String[]{"ss", "support", "server"};
        this.category = new Category("other");
        this.name = "supportserver";
        this.help = "Sends the support server!";
    }

    @Override
    protected void execute(CommandEvent event){
        EmbedBuilder ssEmbed = new EmbedBuilder();
        ssEmbed.appendDescription("[Click me to go to the server!](<" + event.getClient().getServerInvite() + ">)");
        ssEmbed.setColor(CustomColors.tsukiMagenta);

        event.reply(ssEmbed.build());
    }

}
