package me.mdstumpfy123.tsuki.commands.fun;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.mdstumpfy123.tsuki.utils.CustomColors;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.User;

public class EightballCommand extends Command {
    public EightballCommand() {
        this.name = "8ball";
        this.aliases = new String[]{"8b", "eightball"};
        this.guildOnly = false;
        this.arguments = "<question>";
        this.category = new Category("fun");
        this.help = "Gives you the truth on a question!";
    }

    @Override
    protected void execute(CommandEvent event) {
        String[] replies = {"Yes!", "No!", "Maybe!", "Ask a again later.", "Go for it!", "No don't!", "Don't count on it!", "Cannot predict now"};
        String question = event.getArgs();
        User author = event.getMessage().getAuthor();

        if(question == null) {
            event.reply("You didn't give me a question!");
            return;
        }

        EmbedBuilder embed = new EmbedBuilder()
                .setAuthor("Hmm... The 8ball thinks.." + replies[(int)Math.random()*replies.length], null, author.getEffectiveAvatarUrl())
                .setColor(CustomColors.tsukiMagenta);

        event.reply(embed.build());
    }
}
