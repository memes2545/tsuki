package me.mdstumpfy123.tsuki.commands.fun;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.mdstumpfy123.tsuki.utils.CustomColors;
import net.dv8tion.jda.core.EmbedBuilder;

public class ChooseCommand extends Command {
    public ChooseCommand() {
        this.name = "choose";
        this.arguments = "<item>, <item>, ...";
        this.help = "Chooses a random choice!";
        this.category = new Category("fun");
    }

    @Override
    protected void execute(CommandEvent event){
        String[] items = event.getArgs().split("(,)|(,\\s+)");

        if(event.getArgs().isEmpty()) {
            event.reply("You didn't give me any choices!");
        } else if (items.length == 1){
            event.reply("You need to give me more than one choice!");
        } else{
            EmbedBuilder cEmbed = new EmbedBuilder();
            cEmbed.setColor(CustomColors.tsukiMagenta);
            cEmbed.setTitle("I choose " + items[(int)(Math.random()*items.length)] + "!");

            event.reply(cEmbed.build());
        }
    }

}
