package me.mdstumpfy123.tsuki.commands.music;


import me.mdstumpfy123.tsuki.audio.GuildManager;
import me.mdstumpfy123.tsuki.audio.MusicBotSetup;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;

public class PauseCommand extends Command {
    private MusicBotSetup musicBot;
    public PauseCommand(MusicBotSetup musicBot) {
        this.musicBot = musicBot;
        this.name = "pause";
        this.category = new Category("music");
        this.help = "Pauses the currently playing track";
        this.guildOnly = true;
    }

    @Override
    protected void execute(CommandEvent event) {
        GuildManager manager = musicBot.getManagerFromGuild(event.getGuild());
        manager.player.setPaused(true);
        event.reply("Player paused.");
    }
}
