package me.mdstumpfy123.tsuki.commands.music;

import com.jagrosh.jdautilities.command.Command;
import me.mdstumpfy123.tsuki.audio.MusicBotSetup;
import com.jagrosh.jdautilities.command.CommandEvent;

public class ClearCommand extends Command {
    private MusicBotSetup musicBot;
    public ClearCommand(MusicBotSetup musicBot){
        this.musicBot = musicBot;
        this.name = "clear";
        this.help = "Clears the queue";
        this.category = new Category("music");
        this.guildOnly = true;
    }
    @Override
    protected void execute(CommandEvent event) {
        int count = musicBot.getManagerFromGuild(event.getGuild()).queue.size();
        musicBot.getManagerFromGuild(event.getGuild()).queue.clear();
        event.reply("Queue cleared! `"+count+"` songs removed.");
    }
}
