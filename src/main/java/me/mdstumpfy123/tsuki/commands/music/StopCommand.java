package me.mdstumpfy123.tsuki.commands.music;

import me.mdstumpfy123.tsuki.audio.GuildManager;
import me.mdstumpfy123.tsuki.audio.MusicBotSetup;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;

public class StopCommand extends Command {
    private MusicBotSetup musicBot;

    public StopCommand(MusicBotSetup setup) {
        this.musicBot = setup;
        this.name = "stop";
        this.category = new Category("music");
        this.help = "Stops the music player";
        this.aliases = new String[]{"leave", "stp"};
        this.guildOnly= true;
    }

    @Override
    protected void execute(CommandEvent event) {
        GuildManager manager = musicBot.getManagerFromGuild(event.getGuild());
        manager.stop();
    }
}
