package me.mdstumpfy123.tsuki.commands.music;

import me.mdstumpfy123.tsuki.audio.GuildManager;
import me.mdstumpfy123.tsuki.audio.MusicBotSetup;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;

public class ResumeCommand extends Command {
    private MusicBotSetup musicBot;
    public ResumeCommand(MusicBotSetup musicBot){
        this.musicBot = musicBot;
        this.name = "resume";
        this.aliases = new String[]{"unpause"};
        this.category = new Category("music");
        this.help = "Resumes the track if paused";
    }

    @Override
    protected void execute(CommandEvent event) {
        GuildManager manager = musicBot.getManagerFromGuild(event.getGuild());
        if (manager.player.isPaused()) {
            manager.player.setPaused(false);
            event.reply("Player resumed.");
        } else {
            event.reply("Not playing anything.");
        }
    }
}
