package me.mdstumpfy123.tsuki.commands.music;

import me.mdstumpfy123.tsuki.audio.GuildManager;
import me.mdstumpfy123.tsuki.audio.MusicBotSetup;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;

public class ShuffleCommand extends Command {
    MusicBotSetup musicBot;

    public ShuffleCommand(MusicBotSetup musicBot) {
        this.musicBot = musicBot;
        this.name = "shuffle";
        this.help = "Shuffles the queue";
        this.category = new Category("music");
        this.guildOnly = true;
    }

    @Override
    protected void execute(CommandEvent event) {
        GuildManager manager = musicBot.getManagerFromGuild(event.getGuild());
        manager.shuffle();
    }
}
