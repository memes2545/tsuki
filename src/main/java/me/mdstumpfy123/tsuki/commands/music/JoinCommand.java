package me.mdstumpfy123.tsuki.commands.music;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.core.managers.AudioManager;

/**
 * Joins the requester's channel
 */
public class JoinCommand extends Command {
    public JoinCommand() {
        this.name = "join";
        String[] aliases = {"connect"};
        this.category = new Category("music");
        this.aliases = aliases;
        this.guildOnly = true;
        this.help = "Joins a voice channel";
    }

    @Override
    protected void execute(CommandEvent event) {
        AudioManager mng = event.getGuild().getAudioManager();
        if (!mng.isConnected() && !mng.isAttemptingToConnect()) {
            if (!event.getMember().getVoiceState().inVoiceChannel()) {
                event.reply("You are not in a voice channel. please join then try again.");
                return;
            }
            mng.openAudioConnection(event.getMember().getVoiceState().getChannel());
        }
    }
}
