package me.mdstumpfy123.tsuki.commands.music;

import me.mdstumpfy123.tsuki.audio.MusicBotSetup;
import me.mdstumpfy123.tsuki.utils.RoleUtils;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.core.Permission;

import java.util.Objects;

public class SkipCommand extends Command{
    private MusicBotSetup musicBot;

    public SkipCommand(MusicBotSetup musicBot){
        this.musicBot = musicBot;
        this.name = "skip";
        this.help = "Skip the currently playing song.";
        this.category = new Category("music");
        this.guildOnly = true;
    }

    @Override
    protected void execute(CommandEvent event) {
        if (!musicBot.getManagerFromGuild(event.getGuild()).isPlaying()) {
            event.reply("Not playing anything...");
        } else {
            if (!Objects.equals(event.getArgs(), "")) {
                if ((event.getMember().getVoiceState().getChannel().equals(event.getGuild().getAudioManager().getConnectedChannel()))) {
                    int j = Integer.parseInt(event.getArgs());
                    musicBot.getManagerFromGuild(event.getGuild()).skip(event.getMember(), event, j);
                } else {
                    event.reply("Must be connected to the voice channel.");
                }
            } else if ((event.getMember().getVoiceState().getChannel().equals(event.getGuild().getAudioManager().getConnectedChannel()))) {
                musicBot.getManagerFromGuild(event.getGuild()).skip(event.getMember(), event);
            } else  {
                event.reply("Must be connected to the voice channel.");
            }
        }
    }
}
