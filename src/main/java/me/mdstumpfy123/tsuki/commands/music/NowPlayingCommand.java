package me.mdstumpfy123.tsuki.commands.music;

import me.mdstumpfy123.tsuki.audio.GuildManager;
import me.mdstumpfy123.tsuki.audio.MusicBotSetup;
import me.mdstumpfy123.tsuki.utils.TextUtils;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;

public class NowPlayingCommand extends Command {
    private MusicBotSetup musicBot;

    public NowPlayingCommand(MusicBotSetup musicBot) {
        this.musicBot = musicBot;
        this.name = "np";
        this.aliases = new String[]{"playing", "nowplaying"};
        this.category = new Category("music");
        this.help = "Shows the currently playing song.";
        this.guildOnly = true;
    }

    @Override
    protected void execute(CommandEvent event) {
        GuildManager manager = musicBot.getManagerFromGuild(event.getGuild());
        if (!manager.player.isPaused()&&manager.getCurrent()==null) {
            event.reply("Not playing anything...");
        } else {
            event.reply("Now playing: " + TextUtils.trackToString(manager.getCurrent().track) + " at " + TextUtils.durationToString(manager.getPosition()));
        }
    }
}
