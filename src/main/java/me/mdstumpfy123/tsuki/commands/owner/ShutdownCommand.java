package me.mdstumpfy123.tsuki.commands.owner;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;

public class ShutdownCommand extends Command {
    public ShutdownCommand()
    {
        this.name = "shutdown";
        this.help = "Safely shuts off the bot.";
        this.guildOnly = false;
        this.ownerCommand = true;
        this.category = new Category("owner");
    }

    @Override
    protected void execute(CommandEvent event) {
        event.reactWarning();
        event.getJDA().shutdown();
    }
}

