package me.mdstumpfy123.tsuki.commands.owner;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.mdstumpfy123.tsuki.utils.CustomColors;
import net.dv8tion.jda.core.EmbedBuilder;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EvalCommand extends Command {
    public EvalCommand(){
        this.name = "eval";
        this.ownerCommand = true;
        this.category = new Category("owner");
        this.help = "Runs code!";
        this.arguments = "<code>";
    }

    @Override
    protected void execute(CommandEvent event) {
        String code = event.getArgs();

        ScriptEngine se = new ScriptEngineManager().getEngineByName("nashorn");
        se.put("event", event);
        se.put("jda", event.getJDA());
        se.put("channel", event.getTextChannel());
        se.put("message", event.getMessage());
        se.put("guild", event.getGuild());
        try {
            EmbedBuilder eval = new EmbedBuilder();
            eval.addField(":inbox_tray: **Input**", "```java\n"+code+"```", false);
            eval.addField(":outbox_tray: **Output**", "```java\n" +
                    se.eval(Stream.of(code).collect(Collectors.joining("  "))) + "```", false);
            eval.setColor(CustomColors.tsukiMagenta);
            
            event.reply(eval.build());
        } catch (Exception e) {
            event.getChannel().sendMessage("Error:\n```\n" + e + "```").queue();
        }
    }
    }

