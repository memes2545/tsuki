package me.mdstumpfy123.tsuki.commands.general;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.mdstumpfy123.tsuki.utils.CustomColors;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.TextChannel;

public class FeedbackCommand extends Command {
    public FeedbackCommand() {
        this.name = "feedback";
        this.category = new Category("general");
        this.help = "Give feedback!";
        this.arguments = "<feedback>";
    }

    @Override
    protected void execute(CommandEvent event) {
        JDA jda = event.getJDA();
        TextChannel fbChannel = jda.getTextChannelById("430458680492949533");
        Member author = event.getMember();
        String feedback = event.getArgs();

        EmbedBuilder fbEmbed = new EmbedBuilder();
        fbEmbed.setColor(CustomColors.tsukiMagenta);
        fbEmbed.addField("User", author.getUser().getName() + "#" + author.getUser().getDiscriminator(), true);
        fbEmbed.addField("Feedback", feedback, true);
        fbEmbed.setAuthor("New Feedback!", null, event.getMember().getUser().getEffectiveAvatarUrl());

        if(event.getArgs().isEmpty()) {
            event.reply("You need to supply feedback!");
        } else {
            event.reply("Feedback sent!");
            fbChannel.sendMessage(fbEmbed.build()).queue();
        }

    }
}
