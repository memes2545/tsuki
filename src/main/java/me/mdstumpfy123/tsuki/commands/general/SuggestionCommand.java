package me.mdstumpfy123.tsuki.commands.general;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.mdstumpfy123.tsuki.utils.CustomColors;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.TextChannel;

public class SuggestionCommand extends Command {
    public SuggestionCommand() {
        this.name = "suggestion";
        this.category = new Command.Category("general");
        this.help = "Sends a suggestion to the owners!";
        this.arguments = "<suggestion>";
    }

    @Override
    protected void execute(CommandEvent event) {
        JDA jda = event.getJDA();
        TextChannel sugChannel = jda.getTextChannelById("430458706262622210");
        Member author = event.getMember();
        String suggestion = event.getArgs();

        EmbedBuilder sugEmbed = new EmbedBuilder();
        sugEmbed.setColor(CustomColors.tsukiMagenta);
        sugEmbed.addField("User", author.getUser().getName() + "#" + author.getUser().getDiscriminator(), true);
        sugEmbed.addField("Suggestion", suggestion, true);
        sugEmbed.setAuthor("New Suggestion!", null, event.getMember().getUser().getEffectiveAvatarUrl());

        if(event.getArgs().isEmpty()) {
            event.reply("You need to supply a suggestion!");
        } else {
            event.reply("Suggestion sent!");
            sugChannel.sendMessage(sugEmbed.build()).queue();
        }

    }
}
