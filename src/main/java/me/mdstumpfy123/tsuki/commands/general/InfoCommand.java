package me.mdstumpfy123.tsuki.commands.general;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.core.EmbedBuilder;

import java.awt.*;

public class InfoCommand extends Command {
    public InfoCommand() {
        this.name = "info";
        this.aliases = new String[]{"i"};
        this.help = "Shows info about the bot!";
        this.category = new Category("general");
    }

    @Override
    protected void execute(CommandEvent event) {
        EmbedBuilder embed = new EmbedBuilder();
        embed.setTitle("Tsuki Info");
        embed.setColor(Color.pink);
        embed.setThumbnail(event.getSelfUser().getAvatarUrl());
        embed.addField("Guild Count", String.valueOf(event.getJDA().getGuilds().size()), true);
        embed.addField("User Count", String.valueOf(event.getJDA().getUserCache().size()), true);
        embed.addField("Invite", "[invite](https://discordapp.com/api/oauth2/authorize?client_id=430101737769009162&permissions=36768768&scope=bot)", true);
        embed.addField("Support Server", "[join]("+event.getClient().getServerInvite()+")", true);
        embed.addField("Creators", "Memes#2545 & dondish#7155", true);
        embed.addField("Commands", String.valueOf(event.getClient().getCommands().size()), true);
        embed.setFooter("Requested by " + event.getAuthor().getName() + "#" + event.getAuthor().getDiscriminator(), event.getAuthor().getEffectiveAvatarUrl());
        event.reply(embed.build());
    }
}
