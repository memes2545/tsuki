package me.mdstumpfy123.tsuki.commands.general;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.mdstumpfy123.tsuki.utils.CustomColors;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.TextChannel;

public class BugReportCommand extends Command {
    public BugReportCommand() {
        this.name = "bugreport";
        this.category = new Command.Category("general");
        this.help = "Reports a bug!";
        this.arguments = "<bug>";
    }

    @Override
    protected void execute(CommandEvent event) {
        JDA jda = event.getJDA();
        TextChannel brChannel = jda.getTextChannelById("430458621084827648");
        Member author = event.getMember();
        String bugreport = event.getArgs();

        EmbedBuilder brEmbed = new EmbedBuilder();
        brEmbed.setColor(CustomColors.tsukiBug);
        brEmbed.addField("User", author.getUser().getName() + "#" + author.getUser().getDiscriminator(), true);
        brEmbed.addField("Bug", bugreport, true);
        brEmbed.setAuthor("New Bug!", null, event.getMember().getUser().getEffectiveAvatarUrl());

        if(event.getArgs().isEmpty()) {
            event.reply("You need to supply a bug!");
        } else {
            event.reply("Bug Report sent!");
            brChannel.sendMessage(brEmbed.build()).queue();
        }

    }
}
