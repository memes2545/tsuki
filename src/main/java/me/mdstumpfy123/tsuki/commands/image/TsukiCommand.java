package me.mdstumpfy123.tsuki.commands.image;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.mdstumpfy123.tsuki.utils.ImageUtils;
import net.dv8tion.jda.core.entities.User;

import java.util.List;

public class TsukiCommand extends Command {
    public TsukiCommand() {
        this.name = "tsuki";
        this.category = new Category("image");
        this.arguments = "<@user>";
        this.guildOnly = true;
        this.help = "Filters Tsuki upon you";
        this.cooldown = 5;
    }

    @Override
    protected void execute(CommandEvent event) {
        List<User> users = event.getMessage().getMentionedUsers();
        if (users.isEmpty()) {
            event.reply("Please mention a user.");
            return;
        }
        User u = users.get(0);
        ImageUtils.filterImageOnAvatar(event, u.getAvatarUrl(), event.getSelfUser().getAvatarUrl(), 0.2F);
    }
}
