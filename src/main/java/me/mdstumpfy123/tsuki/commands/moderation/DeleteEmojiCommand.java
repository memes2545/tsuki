package me.mdstumpfy123.tsuki.commands.moderation;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Emote;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.managers.GuildController;

import java.io.IOException;
import java.util.List;

public class DeleteEmojiCommand extends Command {
    public DeleteEmojiCommand() {
        this.name = "deleteemoji";
        this.aliases = new String[]{"de"};
        this.arguments = "<name>";
        this.userPermissions = new Permission[]{Permission.MANAGE_EMOTES};
        this.botPermissions = new Permission[]{Permission.MANAGE_EMOTES};
        this.category = new Category("moderation");
        this.help = "Deletes a emoji!";
        this.guildOnly = true;
    }

    @Override
    protected void execute(CommandEvent event) {
        Guild guild = event.getMessage().getGuild();
        GuildController controller = guild.getController();
        List<Message.Attachment> attachments = event.getMessage().getAttachments();
        String emoteName = event.getArgs();
        Emote emoteCheck = guild.getEmotes().stream().filter(e->e.getName().equals(emoteName)).findFirst().orElse(null);

        if (event.getArgs().isEmpty()){
            event.reply("You need to give a emote name!");
            return;
        }

        if(emoteCheck == null) {
            event.reply("That isn't a valid emote!");
            return;
        }

            emoteCheck.delete().queue(success-> {
                event.reply("Deleted " + emoteCheck.getName());
            }, error-> {
                event.reply("Unable to delete the emoji! " + ": " + error + "\n" +
                        "Please report this in our support server " + event.getClient().getServerInvite());
            });
    }
}
