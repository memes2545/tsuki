package me.mdstumpfy123.tsuki.commands.moderation;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.mdstumpfy123.tsuki.utils.CustomColors;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.managers.GuildController;

import java.util.Arrays;
import java.util.List;

public class UnmuteCommand extends Command {
    public UnmuteCommand() {
        this.name = "unmute";
        this.arguments = "<@user> [reason]";
        this.help = "Unmutes the user mentioned!";
        this.category = new Category("moderation");
        this.guildOnly = true;
        this.category = new Command.Category("moderation");
        this.userPermissions = new Permission[]{Permission.MANAGE_ROLES};
        this.botPermissions = new Permission[]{Permission.MANAGE_ROLES};
    }

    @Override
    protected void execute(CommandEvent event) {
        Guild guild = event.getGuild();
        GuildController controller = guild.getController();
        Role muteRole = guild.getRoleCache().stream().filter(r->r.getName().equals("Muted")).findFirst().orElse(null);
        List<Member> mentionedMembers = event.getMessage().getMentionedMembers();
        Member mutedMember = mentionedMembers.get(0);
        Member author = event.getMessage().getMember();
        TextChannel modLogChannel = guild.getTextChannelCache().stream().filter(c->c.getName().equals("mod-log")).findFirst().orElse(null);
        Role mutedRoleonMentioned = mutedMember.getRoles().stream().filter(r->r.getName().equals("Muted")).findFirst().orElse(null);
        String args = event.getArgs();
        final String reason = event.getArgs().split("\\s+").length < 2 ? "No reason found" : String.join(" ", Arrays.copyOfRange(args.split("\\s+"), 1, args.split("\\s+").length));

        if(muteRole == null){
            event.reply("Please make a `Muted` role with `SEND_MESSAGES` disabled on all channels.");
            return;
        }

        if(mentionedMembers.isEmpty()) {
            event.reply("Please mention a user to Mute");
            return;
        }

        if(modLogChannel == null) {
            event.reply("Cannot find a mod-log channel! Please make one\nso I can log!");
            return;
        }

        if(mutedRoleonMentioned==null){
            event.reply("This user isn't muted!");
            return;
        }

        controller.removeSingleRoleFromMember(mutedMember, muteRole).queue(success-> {
            event.reply("Successfully Unmuted " + mutedMember.getUser().getName());
            EmbedBuilder modLog = new EmbedBuilder()
                    .setTitle("Unmute")
                    .appendDescription("Yay!")
                    .setColor(CustomColors.tsukiSuccess)
                    .addField("Moderator", author.getUser().getName() + "#" + author.getUser().getDiscriminator(), true)
                    .addField("Reason", reason, true)
                    .addField("User", mutedMember.getUser().getName() + "#" + mutedMember.getUser().getDiscriminator(), true);
            modLogChannel.sendMessage(modLog.build()).queue();
        }, error-> {
            event.reply("Unable to unmute " + mutedMember.getUser().getName() + ": " + error + "\n" +
                    "Please report this in our support server " + event.getClient().getServerInvite());
        });

    }
}
