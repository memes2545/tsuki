package me.mdstumpfy123.tsuki.commands.moderation;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.managers.GuildController;

public class CreateCategoryCommand extends Command {
    public CreateCategoryCommand() {
        this.name = "createcategory";
        this.aliases = new String[]{"createcata", "cc"};
        this.arguments = "<name>";
        this.userPermissions = new Permission[]{Permission.MANAGE_CHANNEL};
        this.botPermissions = new Permission[]{Permission.MANAGE_CHANNEL};
        this.category = new Category("moderation");
        this.help = "Creates a category!";
        this.guildOnly = true;
    }

    @Override
    protected void execute(CommandEvent event) {
        Guild guild = event.getMessage().getGuild();
        GuildController controller = guild.getController();
        String cName = event.getArgs();

        controller.createCategory(cName).queue(success-> {
            event.reply("Category created!");
        }, error-> {
            event.reply("Unable to create the channel! " + ": " + error + "\n" +
                    "Please report this in our support server " + event.getClient().getServerInvite());
        });
    }
}
