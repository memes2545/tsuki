package me.mdstumpfy123.tsuki.commands.moderation;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.mdstumpfy123.tsuki.utils.CustomColors;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.managers.GuildController;

import java.util.Arrays;
import java.util.List;

public class KickCommand extends Command {
    public KickCommand(){
        this.name = "kick";
        this.arguments = "<@user> [reason]";
        this.help = "Kicks the user mentioned!";
        this.category = new Command.Category("moderation");
        this.guildOnly = true;
        this.category = new Command.Category("moderation");
        this.userPermissions = new Permission[]{Permission.KICK_MEMBERS};
        this.botPermissions = new Permission[]{Permission.KICK_MEMBERS};

    }

    @Override
    protected void execute(CommandEvent event) {
        Guild guild = event.getGuild();
        GuildController controller = guild.getController();
        List<Member> mentionedMembers = event.getMessage().getMentionedMembers();
        Member kickedMember = mentionedMembers.get(0);
        Member author = event.getMessage().getMember();
        TextChannel modLogChannel = guild.getTextChannelCache().stream().filter(c->c.getName().equals("mod-log")).findFirst().orElse(null);
        String args = event.getArgs();
        final String reason = event.getArgs().split("\\s+").length < 2 ? "No reason found" : String.join(" ", Arrays.copyOfRange(args.split("\\s+"), 1, args.split("\\s+").length));

        if(mentionedMembers.isEmpty()){
            event.reply("Please mention a user to kick");
            return;
        }

        if(modLogChannel == null){
            event.reply("Cannot find a mod-log channel! Please make one\nso I can log!");
            return;
        }

        controller.kick(mentionedMembers.get(0)).queue(success->{
            event.reply("Successfully Kicked " + kickedMember.getUser().getName());
            EmbedBuilder modLog = new EmbedBuilder()
                    .setTitle("Kick")
                    .appendDescription("Uh Oh!")
                    .setColor(CustomColors.tsukiMute)
                    .addField("Moderator", author.getUser().getName()+"#"+author.getUser().getDiscriminator(), true)
                    .addField("Reason", reason, true)
                    .addField("User", kickedMember.getUser().getName()+"#"+kickedMember.getUser().getDiscriminator(), true);
            modLogChannel.sendMessage(modLog.build()).queue();
        }, error-> {
            event.reply("Unable to kick " + kickedMember.getUser().getName() + ": " + error + "\n" +
                    "Please report this in our support server " + event.getClient().getServerInvite());
        });

    }
}
