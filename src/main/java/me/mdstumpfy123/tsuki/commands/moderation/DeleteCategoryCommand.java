package me.mdstumpfy123.tsuki.commands.moderation;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Category;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.managers.GuildController;

public class DeleteCategoryCommand extends Command {
    public DeleteCategoryCommand(){
        this.name = "deletecategory";
        this.aliases = new String[]{"deletecata", "dc"};
        this.arguments = "<name>";
        this.userPermissions = new Permission[]{Permission.MANAGE_CHANNEL};
        this.botPermissions = new Permission[]{Permission.MANAGE_CHANNEL};
        this.category = new Command.Category("moderation");
        this.help = "Deletes a category!";
        this.guildOnly = true;
    }

    @Override
    protected void execute(CommandEvent event) {
        Guild guild = event.getMessage().getGuild();
        GuildController controller = guild.getController();
        String cName = event.getArgs();
        net.dv8tion.jda.core.entities.Category cCheckName = guild.getCategories().stream().filter(c->c.getName().equals(cName)).findFirst().orElse(null);

        if(cCheckName == null) {
            event.reply("That isn't a category in the server!");
            return;
        }

        cCheckName.delete().queue(success-> {
            event.reply("Deleted " + cCheckName.getName());
        }, error-> {
            event.reply("Unable to delete the category! " + ": " + error + "\n" +
                    "Please report this in our support server " + event.getClient().getServerInvite());
        });
    }
}
