package me.mdstumpfy123.tsuki.commands.moderation;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.managers.GuildController;

public class DeleteTextChannelCommand extends Command {
    public DeleteTextChannelCommand(){
        this.name = "deletetextchannel";
        this.aliases = new String[]{"deletetc", "dtc"};
        this.arguments = "<name>";
        this.userPermissions = new Permission[]{Permission.MANAGE_CHANNEL};
        this.botPermissions = new Permission[]{Permission.MANAGE_CHANNEL};
        this.category = new Category("moderation");
        this.help = "Deletes a text channel!";
        this.guildOnly = true;
    }

    @Override
    protected void execute(CommandEvent event) {
        Guild guild = event.getMessage().getGuild();
        GuildController controller = guild.getController();
        String tcName = event.getArgs();
        TextChannel tcCheckName = guild.getTextChannels().stream().filter(c->c.getName().equals(tcName)).findFirst().orElse(null);

        if(tcCheckName == null) {
            event.reply("That isn't a text channel in the server!");
            return;
        }

        tcCheckName.delete().queue(success-> {
            event.reply("Deleted " + tcCheckName.getName());
        }, error-> {
            event.reply("Unable to delete the channel! " + ": " + error + "\n" +
                    "Please report this in our support server " + event.getClient().getServerInvite());
        });
    }
}
