package me.mdstumpfy123.tsuki.commands.moderation;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.mdstumpfy123.tsuki.utils.CustomColors;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.managers.GuildController;

import java.util.Arrays;
import java.util.List;

public class SoftbanCommand extends Command {
    public SoftbanCommand(){
        this.name = "softban";
        this.arguments = "<@user> [reason]";
        this.help = "Bans then Unbans the user mentioned!";
        this.category = new Command.Category("moderation");
        this.guildOnly = true;
        this.category = new Command.Category("moderation");
        this.userPermissions = new Permission[]{Permission.KICK_MEMBERS};
        this.botPermissions = new Permission[]{Permission.KICK_MEMBERS};
    }

    @Override
    protected void execute(CommandEvent event) {
        Guild guild = event.getGuild();
        GuildController controller = guild.getController();
        List<Member> mentionedMembers = event.getMessage().getMentionedMembers();
        Member bannedMember = mentionedMembers.get(0);
        Member author = event.getMessage().getMember();
        TextChannel modLogChannel = guild.getTextChannelCache().stream().filter(c->c.getName().equals("mod-log")).findFirst().orElse(null);
        String args = event.getArgs();
        final String reason = event.getArgs().split("\\s+").length < 2 ? "No reason found" : String.join(" ", Arrays.copyOfRange(args.split("\\s+"), 1, args.split("\\s+").length));

        if(mentionedMembers.isEmpty()){
            event.reply("Please mention a user to kick");
            return;
        }

        if(modLogChannel == null){
            event.reply("Cannot find a mod-log channel! Please make one\nso I can log!");
            return;
        }

        controller.ban(bannedMember, 7).queue(done-> {
            controller.unban(bannedMember.getUser()).queue(done2-> {
                event.reply("Softbanned " + bannedMember.getUser().getName());
                EmbedBuilder modLog = new EmbedBuilder()
                        .setTitle("Softban")
                        .appendDescription("Uh Oh!")
                        .setColor(CustomColors.tsukiMute)
                        .addField("Moderator", author.getUser().getName()+"#"+author.getUser().getDiscriminator(), true)
                        .addField("Reason", reason, true)
                        .addField("User", bannedMember.getUser().getName()+"#"+bannedMember.getUser().getDiscriminator(), true);
                modLogChannel.sendMessage(modLog.build()).queue();
            }, error-> {
                event.reply("Error unbanning" + bannedMember.getUser().getName() + ": " + error + "\n" +
                        "Please report this in our support server " + event.getClient().getServerInvite());
            });
        }, error-> {
            event.reply("Error banning" + bannedMember.getUser().getName() + ": " + error + "\n" +
                    "Please report this in our support server " + event.getClient().getServerInvite());
        });

    }
}
