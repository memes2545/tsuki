package me.mdstumpfy123.tsuki.commands.moderation;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.managers.GuildController;

public class CreateTextChannelCommand extends Command {
    public CreateTextChannelCommand() {
        this.name = "createtextchannel";
        this.aliases = new String[]{"createtc", "ctc"};
        this.arguments = "<name>";
        this.userPermissions = new Permission[]{Permission.MANAGE_CHANNEL};
        this.botPermissions = new Permission[]{Permission.MANAGE_CHANNEL};
        this.category = new Category("moderation");
        this.help = "Creates a text channel!";
        this.guildOnly = true;
    }

    @Override
    protected void execute(CommandEvent event) {
        Guild guild = event.getMessage().getGuild();
        GuildController controller = guild.getController();
        String tcName = event.getArgs();

        controller.createTextChannel(tcName).queue(success-> {
            event.reply("Text Channel created!");
        }, error-> {
            event.reply("Unable to create the channel! " + ": " + error + "\n" +
                    "Please report this in our support server " + event.getClient().getServerInvite());
        });
    }
}
