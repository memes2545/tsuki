package me.mdstumpfy123.tsuki.commands.moderation;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.managers.GuildController;
import net.dv8tion.jda.core.entities.Message.Attachment;

import java.io.IOException;
import java.util.List;

public class CreateEmojiCommand extends Command {
    public CreateEmojiCommand() {
        this.name = "createemoji";
        this.aliases = new String[]{"ce"};
        this.arguments = "<name>";
        this.userPermissions = new Permission[]{Permission.MANAGE_EMOTES};
        this.botPermissions = new Permission[]{Permission.MANAGE_EMOTES};
        this.category = new Category("moderation");
        this.help = "Creates a emoji!";
        this.guildOnly = true;
    }

    @Override
    protected void execute(CommandEvent event) {
        Guild guild = event.getMessage().getGuild();
        GuildController controller = guild.getController();
        List<Attachment> attachments = event.getMessage().getAttachments();
        String emoteName = event.getArgs();
        Attachment attachment = attachments.get(0);

        if (attachments.isEmpty()) {
            event.reply("You need to attach a image!");
            return;
        }

        if (event.getArgs().isEmpty()){
            event.reply("You need to give a emote name!");
            return;
        }

        try {
            controller.createEmote(emoteName, attachment.getAsIcon()).queue(success -> {
                event.reply("Emoji created!");
            }, error -> {
                event.reply("Unable to create the emoji! " + ": " + error + "\n" +
                        "Please report this in our support server " + event.getClient().getServerInvite());
            });
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
