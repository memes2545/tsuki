package me.mdstumpfy123.tsuki.commands.moderation;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.VoiceChannel;
import net.dv8tion.jda.core.managers.GuildController;

public class DeleteVoiceChannelCommand extends Command {
    public DeleteVoiceChannelCommand(){
        this.name = "deletevoicechannel";
        this.aliases = new String[]{"deletevc", "dvc"};
        this.arguments = "<name>";
        this.userPermissions = new Permission[]{Permission.MANAGE_CHANNEL};
        this.botPermissions = new Permission[]{Permission.MANAGE_CHANNEL};
        this.category = new Category("moderation");
        this.help = "Deletes a voice channel!";
        this.guildOnly = true;
    }

    @Override
    protected void execute(CommandEvent event) {
        Guild guild = event.getMessage().getGuild();
        GuildController controller = guild.getController();
        String vcName = event.getArgs();
        VoiceChannel vcCheckName = guild.getVoiceChannels().stream().filter(c->c.getName().equals(vcName)).findFirst().orElse(null);

        if(vcCheckName == null) {
            event.reply("That isn't a voice channel in the server!");
            return;
        }

        vcCheckName.delete().queue(success-> {
            event.reply("Deleted " + vcCheckName.getName());
        }, error-> {
            event.reply("Unable to delete the channel! " + ": " + error + "\n" +
                    "Please report this in our support server " + event.getClient().getServerInvite());
        });
    }
}
