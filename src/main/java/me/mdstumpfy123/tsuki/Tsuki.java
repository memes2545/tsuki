package me.mdstumpfy123.tsuki;

import com.jagrosh.jdautilities.command.CommandClientBuilder;
import com.jagrosh.jdautilities.examples.command.GuildlistCommand;
import com.jagrosh.jdautilities.examples.command.PingCommand;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import me.mdstumpfy123.tsuki.api.TsukiEventListener;
import me.mdstumpfy123.tsuki.audio.*;
import me.mdstumpfy123.tsuki.commands.image.TsukiCommand;
import me.mdstumpfy123.tsuki.commands.fun.*;
import me.mdstumpfy123.tsuki.commands.general.*;
import me.mdstumpfy123.tsuki.commands.moderation.*;
import me.mdstumpfy123.tsuki.commands.music.*;
import me.mdstumpfy123.tsuki.commands.other.*;

import me.mdstumpfy123.tsuki.commands.owner.*;
import me.mdstumpfy123.tsuki.events.*;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.OnlineStatus;
import net.dv8tion.jda.core.entities.Game;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.security.auth.login.LoginException;

public class Tsuki {
    public static void main(String[] args) throws LoginException, InterruptedException {
        CommandClientBuilder ccb = new CommandClientBuilder();
        Logger log = LoggerFactory.getLogger(Tsuki.class);
        // Set variables
        ccb.setOwnerId("239790360728043520");
        ccb.setCoOwnerIds("289028991069978624");
        ccb.setPrefix(Config.PREFIX);
        ccb.setAlternativePrefix(Config.MENTIONPREFIX);
        ccb.setServerInvite("https://discord.gg/TUKKfnR");
        ccb.setStatus(OnlineStatus.DO_NOT_DISTURB);

        AudioPlayerManager manager = new DefaultAudioPlayerManager();
        EventWaiter waiter = new EventWaiter();
        MusicBotSetup setup = new MusicBotSetup(manager);
        GuildJoinLogEvent guildJoinEvent = new GuildJoinLogEvent();
        GuildLeaveLogEvent guildLeaveEvent = new GuildLeaveLogEvent();

        log.info("Setting up Commands...");
        // Commands Setup

        // General Commands
        ccb.addCommand(new PingCommand());
        ccb.addCommand(new InfoCommand());
        ccb.addCommand(new FeedbackCommand());
        ccb.addCommand(new BugReportCommand());
        ccb.addCommand(new SuggestionCommand());

        // Music Commands
        ccb.addCommand(new JoinCommand());
        ccb.addCommand(new PlayCommand(waiter, setup));
        ccb.addCommand(new StopCommand(setup));
        ccb.addCommand(new PauseCommand(setup));
        ccb.addCommand(new NowPlayingCommand(setup));
        ccb.addCommand(new QueueCommand(waiter, setup));
        ccb.addCommand(new ResumeCommand(setup));
        ccb.addCommand(new SeekCommand(setup));
        ccb.addCommand(new ExportCommand(setup));
        ccb.addCommand(new ClearCommand(setup));
        ccb.addCommand(new ShuffleCommand(setup));
        ccb.addCommand(new SkipCommand(setup));

        // Other Commands
        ccb.setHelpConsumer(new HelpCommand());
        ccb.addCommand(new DonateCommand());
        ccb.addCommand(new SupportServerCommand());

        // Owner Commands
        ccb.addCommand(new ShutdownCommand());
        ccb.addCommand(new EvalCommand());
        ccb.addCommand(new GuildlistCommand(waiter));

        // Fun Commands
        ccb.addCommand(new ChooseCommand());
        ccb.addCommand(new EightballCommand());

        // Moderation Commands
        ccb.addCommand(new MuteCommand());
        ccb.addCommand(new BanCommand());
        ccb.addCommand(new KickCommand());
        ccb.addCommand(new SoftbanCommand());
        ccb.addCommand(new UnmuteCommand());
        ccb.addCommand(new CreateVoiceChannelCommand());
        ccb.addCommand(new CreateCategoryCommand());
        ccb.addCommand(new CreateTextChannelCommand());
        ccb.addCommand(new CreateEmojiCommand());
        ccb.addCommand(new DeleteTextChannelCommand());
        ccb.addCommand(new DeleteVoiceChannelCommand());
        ccb.addCommand(new DeleteCategoryCommand());
        ccb.addCommand(new DeleteEmojiCommand());

        // Image Commands
        ccb.addCommand(new TsukiCommand());

        log.info("Done setting up commands. Starting JDA...");

        ccb.setListener(new TsukiEventListener());

        JDA jda = new JDABuilder(AccountType.BOT)
                .setToken(Config.TOKEN)
                .addEventListener(ccb.build())
                .addEventListener(waiter)
                .addEventListener(guildJoinEvent, guildLeaveEvent)
                .setAutoReconnect(true)
                .buildBlocking();

        new DBLStatsUpdate(jda);
        jda.getPresence().setGame(Game.watching("for your commands! tsuki.help ["+jda.getGuilds().size()+"]"));

        log.info("Done booting!");

    }
}
